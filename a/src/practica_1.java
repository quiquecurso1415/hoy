import java.io.*;
//1
import java.nio.channels.FileLock;
/**
 * @author Omar Hernandez
 */
public class practica_1{

	/**
	 * @param args - Para la correcta ejecucion de este ejercicio necesitaremos pasarle dos parametros por CMD,
	 * El primer parametro sera el numero de lineas que quieres que escriba y el segundo sera la ruta de creacion del fichero con dichas palabras
	 */
	public static void main(String[] args) {

		String cadena = "";
		int dimension=0,numLineas=0;
		char letraNum;
		File archivo = null;
		RandomAccessFile raf = null;
		FileLock bloqueo = null;

	// hacemos un cast del parametro a int y hacemos el random de letras y de dimension y guardamos las palabras en "cadena"
		numLineas = Integer.parseInt(args[0]);
		for (int i = 0; i < numLineas; i++){

			dimension = (int)(Math.random()*20+1);
			cadena = "";
			for(int c = 0; c != dimension;c++){

				letraNum = (char)(Math.random()*26+65);
				cadena = cadena + letraNum;
				System.out.println(cadena);
			}
			//Preparamos el acceso al fichero
			archivo = new File(args[1]);


			try{
				raf = new RandomAccessFile(archivo,"rwd"); //Abrimos el fichero


				bloqueo = raf.getChannel().lock();
				//bloqueamos el canal de acceso al fichero. Obtenemos el objeto que
				//representa el bloqueo para despu�s poder liberarlo

				raf.seek(archivo.length());
				raf.writeBytes("Palabra "+ (i+1) + "-" + cadena + "\n");
				bloqueo.release(); //Liberamos el bloqueo del canal del fichero
				bloqueo = null;


			} catch (Exception e) {
				e.printStackTrace();
			} finally{
	            try{
	                if( null != raf ) 
	                	raf.close();
	                if( null != bloqueo) 
	                	bloqueo.release();
	            }catch (Exception e2){
	            	System.out.println("Error al cerrar el fichero");
	                System.err.println(e2.toString());
	                System.exit(1);  //Si hay error, finalizamos
	            }
	        }
		}
	}

}
