import java.io.*;

/**
 * 
 * @author Omar Hernandez
 *
 */


public class practica_2 {

	/**
	 * @param Esta practica no va a recibir parametros porque lo unico que va a hacer es
	 * ejecutar la practica 1 en un fichero aleatorio con los bloqueos correspondientes
	 */
	public static void main(String[] args) {

		int conta = 0;
		Process nuevoProceso;
		RandomAccessFile raf = null;
		File archivo = null;
		
		PrintStream ps;
		try {
			ps = new PrintStream(
					new BufferedOutputStream(new FileOutputStream(
							new File("javalog.txt"),true)), true);

			System.setOut(ps);
			
			//Generaremos el contador de las palabras incrementandolas de 10 en 10 por cada instancia
			for (int i = 0; i <=10; i++){
				conta = conta + (i * 10);

			}
			archivo = new File("libro.txt");
			try {
				if (!archivo.exists()){
					archivo.createNewFile(); //Lo creamos
					raf = new RandomAccessFile(archivo,"rw"); //Abrimos el fichero en modo lectura-escritura. 

					raf.writeInt(0); //Escribimos el valor inicial 0
					System.out.println("Creado el fichero.");
				}
			}catch(Exception e){
				System.err.println("Error al crear el fichero");
				System.err.println(e.toString());
			}finally{
				try{ // Nos asegurarnos de que se cierra el fichero.
					if (null != raf)
						raf.close();
				} catch (Exception e2) {
					System.err.println("Error al cerrar el fichero");
					System.err.println(e2.toString());
					System.exit(1); //Si hay error, finalizamos
				}
			}
			//Aqui ejecutaremos el comando para que cree las 550 palabras

			nuevoProceso = Runtime.getRuntime().exec("java -jar practica_1.jar " + conta + " c:\\a\\libro.txt");
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
